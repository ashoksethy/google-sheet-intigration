<?php

$name           = $_POST['name-a'];
$organisation   = $_POST['organisation-a'];
$designation    = $_POST['designation-a'];
$country        = $_POST['geography-a'];
$email          = $_POST['email-a'];


$utmcamp        = $_POST['utm_campaign'];
$utmsource      = $_POST['utm_source'];
$utmmed         = $_POST['utm_medium'];
$utmkey         = $_POST['utm_keywords'];

create_log_file('POST DATA::' . json_encode($_POST));

if (isset($name))
{
    // var_dump($_POST);
    // exit();
   
    
    $url = 'https://script.google.com/macros/s/AKfycbyRus4XEZ10x3A-50qOGQzb-80JEpLZMmhp7mkju3bxHrnon6XoKldV/exec?';
    
    $query = 'Name=' . $name . '&Organisation=' . $organisation .  '&Designation=' . $designation . '&Country=' . $country . '&Email_ID=' . $email . '&utm_campaign=' . $utmcamp . '&utm_source=' . $utmsource . '&utm_medium=' . $utmmed  ;


    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $query); 
    
    $response = curl_exec($ch);
    print_r($response);
    curl_close($ch);

    // to redirect to any URL like TY or home page
    header('location: ');
}


// logs- - --  -- - 
// add file "Logs" in same diretory where PostData is located__ so fopen will be 'Logs/logfile-'

function create_log_file($data) {
	if(!file_exists('Logs/logfile-'.date('Y-m-d').'.txt')){
		fopen('Logs/logfile-'.date('Y-m-d').'.txt', "w");
	}
        $f_data = "======".date('Y-m-d H:i:s')."========".PHP_EOL;
        $f_data .= $data.PHP_EOL;
        $f_data .= "=====================================".PHP_EOL;
	file_put_contents('Logs/logfile-'.date('Y-m-d').'.txt', $f_data.PHP_EOL , FILE_APPEND);
}
?>
